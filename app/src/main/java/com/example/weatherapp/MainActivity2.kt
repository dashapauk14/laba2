package com.example.weatherapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView


class MainActivity2 : AppCompatActivity() {
    private lateinit var imageView:ImageView
    private lateinit var txt1:TextView
    private lateinit var txt2:TextView
    private lateinit var button2: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        imageView = findViewById(R.id.imageView)

        txt1 = findViewById(R.id.textView4)
        txt2 = findViewById(R.id.textView5)
        button2 = findViewById(R.id.button2)

        val lang = intent.getBooleanExtra("lang", false)

        val desc = intent.getStringExtra("data1")
        val temp = intent.getStringExtra("data2")
        if(desc!!.toLowerCase().contains("дождь"))
            imageView.setImageResource(R.drawable.rain)
        else
            if(desc!!.toLowerCase().contains("ясно") || desc!!.toLowerCase().contains("sun"))
                imageView.setImageResource(R.drawable.sun)
        else
            if(desc!!.toLowerCase().contains("снег") || desc!!.toLowerCase().contains("snow"))
                imageView.setImageResource(R.drawable.snow)
            else
                if(desc!!.toLowerCase().contains("туман") || desc!!.toLowerCase().contains("fog"))
                    imageView.setImageResource(R.drawable.fog)
                else
                    if(desc!!.toLowerCase().contains("облачн")|| desc!!.toLowerCase().contains("cloud"))
                        imageView.setImageResource(R.drawable.cloud)
                    else
                        imageView.setImageResource(R.drawable.undef)
        if(!lang) {
            txt1.setText("Информация о погоде: $desc")
            txt2.setText("Температура: $temp")
            button2.setText("Об авторе")
        }
        else
        {
            txt1.setText("Information of weather: $desc")
            txt2.setText("Temperature: $temp")
            button2.setText("Information about author")
        }

        button2.setOnClickListener {
            var intent = Intent(baseContext, MainActivity3::class.java)
            startActivity(intent)
        }
    }
}