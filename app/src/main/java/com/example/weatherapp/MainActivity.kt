package com.example.weatherapp

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.widget.Button
import android.widget.Switch
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import org.jetbrains.anko.activityUiThread
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.net.URL


class MainActivity : AppCompatActivity() {
    private lateinit var plainText: TextView;
    private lateinit var button: Button;
    private lateinit var city: String
    private lateinit var switch: Switch
    private lateinit var text2: TextView
    private lateinit var text3: TextView
    private var key: String = "53b88e49f7a16c5b22dcab85cef4a9f1"
    private var url: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)


        plainText = findViewById(R.id.editTextTextPersonName)
        button = findViewById(R.id.button)
        switch = findViewById(R.id.switch1)
        text2 = findViewById(R.id.textView2)
        text3 = findViewById(R.id.textView3)

        switch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                text2.text = "Weather Application"
                text3.text = "Enter your city:"
                plainText.hint = "Enter city"
                button.text = "Check weather"
            } else {
                text2.text = "Приложение погода"
                text3.text = "Введите название своего города:"
                plainText.hint = "Введите город"
                button.text = "Узнать погоду"
            }
        }

        button.setOnClickListener {

            doAsync {
                city = plainText.text.toString()
                if(switch.isChecked)
                    url = "https://api.openweathermap.org/data/2.5/weather?q=$city&appid=$key"
                else
                    url = "https://api.openweathermap.org/data/2.5/weather?q=$city&appid=$key&units=metric&lang=Ru"
                val apiResponse = URL(url).readText()
                val weather = JSONObject(apiResponse).getJSONArray("weather")
                val desc = weather.getJSONObject(0).getString("description")

                val main = JSONObject(apiResponse).getJSONObject("main")
                val temp = main.getString("temp")

                activityUiThread {
                    val intent = Intent(baseContext, MainActivity2::class.java)
                    intent.putExtra("data1", desc)
                    intent.putExtra("data2", temp)
                    intent.putExtra("lang", switch.isChecked)
                    startActivity(intent)

                }


            }

        }


    }
}